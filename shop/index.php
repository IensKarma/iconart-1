<?php
// Version
define('VERSION', '2.3.0.2');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Configuration
// Localhost
if ($_SERVER['HTTP_HOST'] == 'iconart.local')
{
    if (is_file('config_local.php')) {
        require_once('config_local.php');
    }
}
// DEV
elseif ($_SERVER['HTTP_HOST'] == 'iconart.artwebit.ru')
{
    if (is_file('config_artwebit.php')) {
        require_once('config_artwebit.php');
    }
}
// Live server http://shop.icon-art.ru/
elseif ($_SERVER['HTTP_HOST'] == 'shop.icon-art.ru')
{
    if (is_file('config_live.php')) {
        require_once('config_live.php');
    }
}

//testchange
// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// VirtualQMOD
require_once('./vqmod/vqmod.php');
VQMod::bootup();

// VQMODDED Startup
require_once(VQMod::modCheck(DIR_SYSTEM . 'startup.php'));

start('catalog');