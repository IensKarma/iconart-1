<?php
// Heading
$_['heading_title'] 		= 'NewsBlog - Category List';

// Text
$_['text_success'] 			= 'Category list updated!';
$_['text_list'] 			= 'List Categories';
$_['text_add'] 				= 'Add Category';
$_['text_edit'] 			= 'Edit category';
$_['text_default'] 			= 'The main shop';

// Column
$_['column_name'] 			= 'The name of the category';
$_['column_count_elements'] = 'Number of materials';
$_['column_sort_order'] 	= 'Sort order';
$_['column_action'] 		= 'Action';

// Entry
$_['entry_name'] 			= 'The name of the category:';
$_['entry_description'] 	= 'Description';
$_['entry_meta_title'] 		= 'HTML-tag Title';
$_['entry_meta_h1'] 		= 'HTML-tag H1';
$_['entry_meta_description']= 'Meta tag Description';
$_['entry_meta_keyword'] 	= 'Meta tag Keywords';

$_['entry_parent'] 			= 'Parent category';
$_['entry_store'] 			= 'Stores';
$_['entry_keyword'] 		= 'SEO URL:';
$_['entry_image'] 			= 'Image';
$_['entry_show_preview'] 	= 'Show summary in full article:';
$_['entry_sort_order'] 		= 'Sort by:';
$_['entry_status'] 			= 'Status';
$_['entry_limit']			= 'The amount of material on the page:';
$_['entry_sort_by'] 		= 'Sort the materials';

$_['entry_template_category']	= 'Template output categories:';
$_['entry_template_article'] 	= 'Article Template output:';
$_['entry_layout'] 				= 'Choose a layout:';

// Help
$_['help_keyword'] 				= 'Change the spaces on the dash. Must be unique throughout the system.';
$_['help_limit'] 				= 'In order to do not display materials, set to 0.';
$_['help_template_category'] 	= 'Specify the file name of the output template materials category. The file must be in the /catalog/view/theme/[template name]/template/newsblog/';
$_['help_template_article'] 	= 'Specify the file name to the template display a detailed description of the article. The file must be in the /catalog/view/theme/[template name]/template/newsblog/';

// Error
$_['error_keyword'] 			= 'This SEO keyword is already in use';
$_['error_meta_title'] 			= 'Title Meta tag should be between 3 and 255 characters!';
$_['error_name'] 				= 'The category name must be between 2 and 32 characters!';
$_['error_permission'] 			= 'You are not authorized to change the category!';
$_['error_warning'] 			= 'Carefully check the form for errors!';

// Sort
$_['sort_by_sort_order'] 		= 'by the order';
$_['sort_by_date_available'] 	= 'by publication date';
$_['sort_by_name'] 				= 'by name';
$_['sort_direction_desc'] 		= 'descending';
$_['sort_direction_asc'] 		= 'ascending';

// Placeholder
$_['placeholder_template_category'] = 'By default category.tpl';
$_['placeholder_template_article'] 	= 'By default article.tpl';