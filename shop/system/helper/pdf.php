<?php

require_once DIR_SYSTEM . 'library/mpdf/mpdf.php';

function createPdf($html, $name) {
    $pdf = new mPDF();
    $pdf->WriteHTML($html);
    $pdf->Output($name, 'F');
}