<?php
// Text
$_['text_image']	= 'Фото';
$_['text_name']		= 'Название';
$_['text_quantity']	= 'Количество';
$_['text_price']	= 'Цена';
$_['text_total']	= 'Всего';
$_['text_points']       = 'Бонусные баллы: %s';