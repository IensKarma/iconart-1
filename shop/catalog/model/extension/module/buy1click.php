<?php

class ModelExtensionModuleBuy1click extends Model {

    public function register($data) {
        $sql = 'INSERT INTO mod_buy1click SET ';
        foreach ($data as $k => $v) {
            $sql .= '`' . $this->db->escape($k) . '` = ' . "'" . $this->db->escape($v) . "', ";
        }
        $sql = mb_substr($sql, 0, -2);
        $query = $this->db->query($sql);
        $this->sendEmail($data);
    }
    
    public function registercart($data) {      
        $this->sendEmailCart($data);
    }

    protected function sendEmailCart($params) {
        $this->load->model('catalog/product');
        
        $text = 'Имя: ' . $params['name'] . '<br>';
        $text .= 'Телефон: ' . $params['phone'] . '<br>';
         $text .= 'Корзина: ' . '<br>';
        $products = $this->cart->getProducts();

		foreach ($products as $product) {
	        $info = $this->model_catalog_product->getProduct( $product['product_id']);
	        $href = $this->url->link('product/product', 'product_id=' . $product['product_id']);
			$text .= 'Продукт: <a href="' . $href . '">#' . $params['product_id'] . ' ' . $info['name'] . '</a> Цена: ' . $product['total'] . ' руб. <br>';
        }
        
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject('Купить в 1 клик. Новая заявка.');
        $mail->setHtml($text);
        $mail->send();
        $emails = explode(',', $this->config->get('config_alert_email'));

        foreach ($emails as $email) {
            if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $mail->setTo($email);
                $mail->send();
            }
        }
    }
    protected function sendEmail($params) {
        $this->load->model('catalog/product');
        $info = $this->model_catalog_product->getProduct((int) $params['product_id']);
        $text = 'Имя: ' . $params['name'] . PHP_EOL;
        $text .= 'Телефон: ' . $params['phone'] . PHP_EOL;
        $text .= 'Продукт: #' . $params['product_id'] . ' ' . $info['name'];
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject('Купить в 1 клик. Новая заявка.');
        $mail->setText($text);
        $mail->send();
        $emails = explode(',', $this->config->get('config_alert_email'));

        foreach ($emails as $email) {
            if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $mail->setTo($email);
                $mail->send();
            }
        }
    }

}
