<ul class="sidebar">
    <?php foreach ($categories as $category) { ?>
    <?php if ($category['category_id'] == $category_id) { ?>
    <li><a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a></li>
    <?php if ($category['children']) { ?>
    <ul>
        <?php foreach ($category['children'] as $child) { ?>
        <?php if ($child['category_id'] == $child_id) { ?>
        <li><a href="<?php echo $child['href']; ?>" class="active">- <?php echo $child['name']; ?></a></li>
        <?php } else { ?>
        <li><a href="<?php echo $child['href']; ?>">- <?php echo $child['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
    </ul>
    <?php } ?>
    <?php } else { ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
    <?php } ?>
    <?php } ?>
</ul>