<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php if ($column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><a><?php echo $heading_title; ?></h1>
      <?php foreach($articles as $article) { ?>
      <div class="row article-row">
          <?php if($article['image']) { ?>
            <div class="image">
                <a href="<?php echo $article['href']?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" /></a>
            </div>
          <?php } ?>
          <a href="<?php echo $article['href']?>"><h3><?php echo $article['name']; ?></h3></a>
          <div class="preview"><?php echo html_entity_decode($article['preview'], ENT_QUOTES, 'UTF-8'); ?></div>
          <div class="links"><a href="<?php echo $article['href']?>">Подробнее...</a></div>
      </div>
      <?php } ?>
      <?php echo $pagination;?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>