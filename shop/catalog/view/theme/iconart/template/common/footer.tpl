</div>
</div>
</div>
<a id="toTop">
    <p>Наверх</p>
</a>
<div class="clear"></div>
<div class="mobile">
    <div>
        <a href="/mailing/" title="Подписка Icon-Art.Ru" id="mailing">Рассылк@</a>
    </div>
    <div id="social-buttons">
        <a href="https://www.facebook.com/PravoslavEveryDay" target="_blank">
            <img src="http://static.icon-art.ru/images/fb.png">
        </a>
        <a href="http://vk.com/club57219595" target="_blank">
            <img src="http://static.icon-art.ru/images/vk.png">
        </a>
        <div class="clear"></div>
        <a class="instagram" target="blank" href="http://instagram.com/ikonopisnayamasterskaei">
            <img src="http://static.icon-art.ru/images/inst.png">
        </a>
        <a class="youtube" target="blank" href="http://www.youtube.com/channel/UCb3L75mMMguWG6LTX-zV0nA/videos">
            <img src="http://static.icon-art.ru/images/yout.png">
        </a>
        <a class="goog" target="blank" href="https://plus.google.com/114100181042973133851/photos">
            <img src="http://static.icon-art.ru/images/gplus.png">
        </a>
    </div>
    <div class="soc_seti">
        <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
        <div class="yashare-auto-init" data-yasharel10n="ru" data-yasharetype="icon" data-yasharequickservices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
        <div class="like">
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
            <script type="text/javascript">VK.init({apiId: 4742436, onlyWidgets: true});</script>
            <div id="vk_like"></div>
            <script type="text/javascript">VK.Widgets.Like("vk_like", {type: "button"});</script>
            <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ficon-art.ru&width&layout=button_count&action=like&show_faces=true&share=false&height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowtransparency="true"></iframe>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="foot">
    <div class="map">
        <div class="map_innner">
            <a href="/feedback/index.html">
                <img src="http://static.icon-art.ru/images/map.jpg" width="308" height="162">
            </a>
        </div>
    </div>
    <div class="contacts">
        <div class="contacts_head">
            <p>© 2015 «Иконописная мастерская Екатерины Ильинской».<br /><br />Все права на данной странице защищены</p>
        </div>
        <div class="con_left">
            <div class="con_left_inner">
                <p>Звоните:</p>
                <span>
                    <a href="tel:+74956425516">+7 (495) 642-55-16</a>
                    <br />
                    <br />
                    <a href="tel:+74955341980">+7 (926) 534-19-80</a>
                </span>
                <div class="left_bottom">
                    <p>г. Москва, м. Спортивная, ул. Кооперативная, д.4,  корп.9, подъезд 2,  цокольный этаж.</p>
                </div>
            </div>
        </div>
        <div class="con_right">
            <div class="con_right_inner">
                <p>
                    Пн-Пт: с 9:00 до 20:00 Сб: с 12:00 до 17:00 Вc - Выходной.
                    <br />
                    <br />
                    Так же по предварительной договорённости мы примем Вас в любое удобное для вас время.
                </p>
            </div>
        </div>
    </div>
    <?php /* <div class="other">
        <div class="visa_mcard">
            <div class="visa">
                <img src="http://static.icon-art.ru/images/visa.png">
            </div>
            <div class="mcard">
                <img src="http://static.icon-art.ru/images/mcard.png">
            </div>
        </div>
        <div class="fbvk"></div>
        <div class="counter">
            <div id="counters">
                <script type="text/javascript">
                    (function (d, w, c) {
                        (w[c] = w[c] || []).push(function () {
                            try {
                                w.yaCounter19021459 = new Ya.Metrika({id: 19021459,
                                    webvisor: true,
                                    clickmap: true,
                                    trackLinks: true,
                                    accurateTrackBounce: true});
                            } catch (e) {
                            }
                        });

                        var n = d.getElementsByTagName("script")[0],
                                s = d.createElement("script"),
                                f = function () {
                                    n.parentNode.insertBefore(s, n);
                                };
                        s.type = "text/javascript";
                        s.async = true;
                        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                        if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f, false);
                        } else {
                            f();
                        }
                    })(document, window, "yandex_metrika_callbacks");
                </script>
                <noscript><div><img src="//mc.yandex.ru/watch/19021459" style="position:absolute; left:-9999px;" alt=""></img></div></noscript>
                <script type="text/javascript">document.write("<a href='http://www.liveinternet.ru/click' " + "target=_blank><img src='http://counter.yadro.ru/hit?t53.6;r" + escape(document.referrer) + ((typeof (screen) == "undefined") ? "" : ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) + ";i" + escape("Жж" + document.title.substring(0, 80)) + ";" + Math.random() + "' alt='' title='LiveInternet: показано число просмотров и посетителей за 24 часа' " + "border=0 width=88 height=31><\/a>")</script><a href="http://www.liveinternet.ru/click" target="_blank"></a><a href="http://top100.rambler.ru/top100/"></a><a href="http://top100.rambler.ru/top100/" target="_blank"></a>
            </div>
        </div>
        <div class="inst"></div>
    </div> */ ?>
</div>
<div class="lb"></div>
<div class="top"></div>
<div class="left1"></div>
<div class="bottom"></div>
<div class="right1"></div>
<div class="rb"></div>
</div>
</div>
</div>
</body>
</html>