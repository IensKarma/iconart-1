<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:php="http://php.net/xsl">
    <head>
        <link rel="icon" href="/favicon.ico?v2" type="image/x-icon"></link>
        <link rel="shortcut icon" href="/favicon.ico?v2" type="image/x-icon"></link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <meta name="yandex-verification" content="455787739c3c8934"></meta>
        <title>Иконописная мастерская Екатерины Ильинской в Москве, Иконы на заказ</title>
        <meta name="keywords" content="Иконописная мастерская, икона, купить икону, икона на заказ, иконы в наличии, икона в подарок"></meta>
        <meta name="description" content="Предлагаем вам создать уникальное произведение искусства, настоящую семейную реликвию-икону для вашей семьи"></meta>
        <link href="/catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <link href="/catalog/view/javascript/jquery/magnific/magnific-popup.css" type="text/css" rel="stylesheet" media="screen" />
        <script src="/catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <script src="/catalog/view/javascript/jquery/jquery.nouislider.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/catalog/view/theme/iconart/stylesheet/nouislider.css" />
        <link href="/catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="/catalog/view/theme/iconart/css/style_screen.css"></link>
        <link rel="stylesheet" type="text/css" href="/catalog/view/theme/iconart/css/style_mob.css" media="all"></link>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"></link>
        <?php foreach ($styles as $style) { ?>
            <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <link href="/catalog/view/theme/iconart/stylesheet/stylesheet.css" rel="stylesheet" />
        <script src="/catalog/view/javascript/common.js" type="text/javascript"></script>
        <script src="/catalog/view/javascript/custom.js" type="text/javascript"></script>
        <?php foreach ($links as $link) { ?>
            <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <?php foreach ($scripts as $script) { ?>
            <script src="<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>
        <meta name="viewport" content="width=450px"></meta>
        <meta name="yandex-verification" content="455787739c3c8934"></meta>
        <meta name="google-site-verification" content="eTycel61Ca7aVj4PmI8SdMMAn1KoSK6Ql2_IERNZndA"></meta>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-49635812-1', 'icon-art.ru');
            ga('send', 'pageview');

        </script>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?105"></script>
        <script type="text/javascript" src="/catalog/view/theme/iconart/js/tabs.v.1.2.js"></script>
        <script type="text/javascript">

            $(function () {

                $(window).scroll(function () {

                    if ($(this).scrollTop() != 0) {

                        $('#toTop').fadeIn();

                    } else {

                        $('#toTop').fadeOut();

                    }

                });

                $('#toTop').click(function () {

                    $('body,html').animate({scrollTop: 0}, 800);

                });

            });

        </script>
    </head>
    <body>
        <div class="snap-drawers">
            <div class="snap-drawer snap-drawer-left">
                <div>
                    <strong>Главное меню</strong>
                    <ul>
                        <li><a href="/">Главная</a></li>
                        <li><a href="/Ikony_v_nalichii.html">Иконы в наличии</a></li>
                        <li><a href="/Alfavit.html">Алфавитный указатель</a></li>
                        <li><a href="/calendar/Pokrovitel.html">Узнай своего покровителя</a></li>
                        <li><a href="/feedback/index.html">Контакты</a></li>
                    </ul>
                    <strong class="lower-h3">Категории</strong>
                    <ul>
                        <li><a href="/calendar">Православный календарь</a></li>
                        <li><a href="/Ikona_v_podarok.html">Икона в подарок</a></li>
                        <li><a href="/Ikony_v_nalichii.html">Иконы в наличии</a></li>
                        <li><a href="/Specialnye_predlozhenija.html">Спецпредложения</a></li>
                        <li><a href="/Kak_sdelat_zakaz.html">Как сделать заказ</a></li>
                        <li><a href="/Our_Recent_Work.html">Наши новые работы</a></li>
                        <li><a href="/Mernaja_ikona.html">Мерная икона</a></li>
                        <li><a href="/Semejjnye_ikony.html">Семейные иконы</a></li>
                        <li><a href="/Venchalnye_ikony.html">Венчальные иконы</a></li>
                        <li><a href="/Imennye_ikony.html">Именная икона</a></li>
                        <li><a href="/Ikony_Bogorodicy.html">Иконы Богородицы</a></li>
                        <li><a href="/Ikony_Spasitelja.html">Иконы Спасителя</a></li>
                        <li><a href="/Otzyvy_zakazchikov.html">Отзывы</a></li>
                        <li><a href="/Skladni_iz_karelskojj_berezy.html">Складни</a></li>
                        <li><a href="/Ikony_s_dragocennymi_kamnjami.html">Иконы с драгоценными камнями</a></li>
                        <li><a href="/Chudotvornye_ikony.html">Чудотворные иконы</a></li>
                        <li><a href="/Zhivopisnye_ikony.html">Живописные иконы</a></li>
                        <li><a href="/Restavracija.html">Реставрация</a></li>
                        <li><a href="/Napisanie_ikon_pod_oklad.html">Написание икон под оклад</a></li>
                        <li><a href="/Kioty_i_firmennye_korobki-shkatulki.html">Киоты и шкатулки</a></li>
                        <li><a href="/Domashnijj_ikonostas.html">Домашний иконостас</a></li>
                        <li><a href="/Doski_dlja_ikon.html">Доски для икон</a></li>
                        <li><a href="/Chto_podarit_na_rozhdestvo.html">Подарок на Рождество</a></li>
                        <li><a href="/Chto_podarit_na_Paskhu.html">Что подарить на Пасху</a></li>
                        <li><a href="/O_masterskojj.html">О мастерской</a></li>
                        <li><a href="/Rabota_v_khramakh.html">Работа в храмах</a></li>
                        <li><a href="/Ikona.html">Икона</a></li>
                        <li><a href="/Izdatelstvo.html">Издательская деятельность</a></li>
                        <li><a href="/Obuchenie_ikonopisi.html">Обучение иконописи</a></li>
                        <li><a href="/Video.html">Видео и публикации</a></li>
                        <li><a href="http://forum.icon-art.ru/">Форум</a></li>
                        <li><a href="/feedback/index.html">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <a href="#" id="open-left"></a>
        <div class="wrapper snap-content" id="content">
            <a name="top"></a>
            <div class="cntnr">
                <div class="head">
                    <div class="logo"><img src="http://static.icon-art.ru/images/logo.png" alt="Логотип мастерской Екатерины Ильинской"></img></div>
                    <div class="cont">
                        <div class="contct">
                            <p>г. Москва, ул. Кооперативная, д.4,<br></br> корп.9, под.2, цокольный этаж.</p>
                            <span>email:<a href="mailto:info@icon-art.ru"> info@icon-art.ru</a></span>
                            <p><a href="tel:+74956425516">+7 (495) 642-55-16</a>, <a href="tel:+79265341980">+7 (926) 534-19-80</a></p>
                        </div>
                        <div class="search">
                            <form action="/search/" method="get">
                                <input type="text" name="search" value=""></input>
                            </form>
                        </div>
                        <div class="cart">
                            <div class="btn-group">
                                <a class="btn btn-default" href="javascript:void(0);" onclick="cart.showPopup();" data-toggle="tooltip" type="button" data-original-title="Корзина покупок"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                <a class="btn btn-default" href="<?php echo $account; ?>" data-toggle="tooltip" type="button" data-original-title="Личный кабинет" ><i class="fa fa-user" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="lup">
                            <img src="http://static.icon-art.ru/images/lupa.png"></img>
                        </div>
                        <div class="buttons"></div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="main_block">
                    <div class="hed">
                        <div class="l"></div>
                        <div class="nav">
                            <div class="left_part">
                                <img src="http://static.icon-art.ru/images/left-fig.png"></img>
                            </div>
                            <div class="nav_center">
                                <ul class="menu_main">
                                    <?php echo $header_menu; ?>  
                                </ul>
                            </div>
                            <div class="right_part">
                                <img src="http://static.icon-art.ru/images/right-fig.png"></img>
                            </div>
                        </div>
                        <div class="r"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="cont_wrapp">
                        <div class="left_side">
                            <br />
                            <?php echo $column_left; ?>
                            <div class="clear"></div>
                        </div>
                        <div class="main">
                            <div class="main_wrapper">