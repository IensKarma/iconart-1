<div class="added-popup col-sm-4">
    <div class="background">
    <h2><?php echo $added_title; ?></h2>
    <div class="image"><?php if($image) { ?><img src="<?php echo $image; ?>" alt='<?php echo $title; ?>' title='<?php echo $title; ?>' /><?php } ?></div>
    <div class='title'><?php echo $title; ?></div>
    <div class='cart-totals panel panel-default'>
        <div class="panel-body">
            <?php echo $total; ?>
            <p><a href='<?php echo $cartLink?>'><?php echo $text_cart_link; ?></a></p>
        </div>
    </div>
    <?php if(!empty($recommendations)) { ?>
    <div class="recommendations">
        <h4><?php echo $text_recommendations; ?>:</h4>
        <table>
            <?php foreach($recommendations as $product) { ?>
            <tr>
                <td><?php if($product['thumb']) { ?><img src="<?php echo $product['thumb']?>" alt="<?php echo $product['title']; ?>" title="<?php echo $product['title']; ?>" /><?php } ?></td>
                <td><a href="<?php echo $product['href']; ?>"><?php echo $product['title']; ?></a></td>
                <td><?php echo $product['price']; ?></td>
                <td><input class="form-control qty" id="qty<?php echo $product['product_id']; ?>" type="number" min="1" value="1" size="4" /></td>
                <td><a class="btn btn-default" onclick="cart.addFromPopup(<?php echo $product['product_id']; ?>);"><?php echo $text_add_to_cart; ?></a></td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <?php } ?>
    </div>
</div>