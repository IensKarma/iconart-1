cart.addWithPopup = function (product_id, quantity) {
    $.ajax({
        url: 'index.php?route=checkout/cart/addPopup',
        type: 'post',
        data: 'product_id=' + product_id + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1),
        success: function (a) {
            if(a.redirect) {
                window.location.href = a.redirect.replace('&amp;', '&');
            } else {
                $.magnificPopup.open({
                    items: {
                        src: a.popup,
                        type: 'inline'
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

cart.addFromPopup = function (productId) {
    var qty = $('#qty' + productId).val();
    cart.addWithPopup(productId, qty);
}

cart.showPopup = function () {
    $.ajax({
        url: 'index.php?route=common/cart/info',
        type: 'get',
        success: function (html) {
            $.magnificPopup.open({
                items: {
                    src: html,
                    type: 'inline'
                }
            });
        }
    });

}

cart.removePopup = function (key) {
    $.ajax({
        url: 'index.php?route=checkout/cart/remove',
        type: 'post',
        data: 'key=' + key,
        dataType: 'json',
        beforeSend: function () {
            $('#cart > button').button('loading');
        },
        complete: function () {
            $('#cart > button').button('reset');
        },
        success: function (json) {
            cart.showPopup();
        }
    });
}