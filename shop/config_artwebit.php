<?php
// HTTP
define('HTTP_SERVER', 'http://iconart.artwebit.ru/');

// HTTPS
define('HTTPS_SERVER', 'http://iconart.artwebit.ru/');

// DIR
define('DIR_APPLICATION', $_SERVER['DOCUMENT_ROOT'].'/catalog/');
define('DIR_SYSTEM', $_SERVER['DOCUMENT_ROOT'].'/system/');
define('DIR_LANGUAGE', $_SERVER['DOCUMENT_ROOT'].'/catalog/language/');
define('DIR_TEMPLATE', $_SERVER['DOCUMENT_ROOT'].'/catalog/view/theme/');
define('DIR_CONFIG', $_SERVER['DOCUMENT_ROOT'].'/system/config/');
define('DIR_IMAGE', $_SERVER['DOCUMENT_ROOT'].'/image/');
define('DIR_CACHE', $_SERVER['DOCUMENT_ROOT'].'/system/storage/cache/');
define('DIR_DOWNLOAD', $_SERVER['DOCUMENT_ROOT'].'/system/storage/download/');
define('DIR_LOGS', $_SERVER['DOCUMENT_ROOT'].'/system/storage/logs/');
define('DIR_MODIFICATION', $_SERVER['DOCUMENT_ROOT'].'/system/storage/modification/');
define('DIR_UPLOAD', $_SERVER['DOCUMENT_ROOT'].'/system/storage/upload/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'user_iconart');
define('DB_PASSWORD', '1efrKKvxZq');
define('DB_DATABASE', 'user_iconart');
define('DB_PREFIX', 'oc_');